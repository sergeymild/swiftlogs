//
//  KVO.h
//  KVO
//
//  Created by Sergei Golishnikov on 26/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for KVO.
FOUNDATION_EXPORT double KVOVersionNumber;

//! Project version string for KVO.
FOUNDATION_EXPORT const unsigned char KVOVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KVO/PublicHeader.h>


