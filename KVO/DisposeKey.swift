//
//  DisposeKey.swift
//  ConnectClub
//
//  Created by Sergei Golishnikov on 26/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation

public class DisposeKey {
    private var observations: [NSKeyValueObservation] = []
    private var _observations: [KVOObservation] = []
    
    public init() {}
    
    public func inser(observation: NSKeyValueObservation) {
        self.observations.append(observation)
    }
    
    public func inser(observation: KVOObservation) {
        self._observations.append(observation)
    }
    
    deinit {
        observations.forEach { $0.invalidate() }
        observations.removeAll()
        
        _observations.forEach { $0.invalidate() }
        _observations.removeAll()
    }
}

public extension NSKeyValueObservation {
    func disposed(by bag: DisposeKey) {
        bag.inser(observation: self)
    }
}

public extension KVOObservation {
    
    func disposed(by bag: DisposeKey) {
        bag.inser(observation: self)
    }
}
