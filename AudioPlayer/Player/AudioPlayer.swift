//  AudioPlayer.swift
//  Created by Yaroslav on 10.01.16

import Foundation
import AVFoundation
import MediaPlayer
import KVO

public extension Notification.Name {
    static let nextTrack = Notification.Name("playNextSong")
    static let previousTrack = Notification.Name("playPreviousSong")
    static let playTrackAtIndex = Notification.Name("playTrackAtIndex")
    static let didShowMainPlayerClick = Notification.Name("didShowMainPlayerClick")
    static let audioDidChangeTime = Notification.Name("audioDidChangeTime")
    static let willPlayAudio = Notification.Name("willPlayAudio")
}

public class AudioPlayer: NSObject {
    static let shared = AudioPlayer()
    
    private(set) var index = 0
    internal var currentAudio: AudioModel?

    private(set) var player: AVPlayer!
    private var _playlist: [AudioModel] = []
    private var timeObserber: Any?
    private var _playerRateObserver: KVOSingleObservation? = nil
    
    var playlist: [AudioModel] { _playlist }
    
    //MARK: - Time Observer
    
    private var currentTime: Double = 0
    @objc
    dynamic var isPlaying: Bool = false
    
    private  func addTimeObeserver() {
        let interval = CMTime(seconds: 1, preferredTimescale: 1)
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishTrackPlaying),
            name: Notification.Name.AVPlayerItemDidPlayToEndTime,
            object: player.currentItem)
        
        _playerRateObserver?.invalidate()
        _playerRateObserver = player.kvo.valueObserve(\.rate) { [weak self] (rate) in
            self?.isPlaying = rate == 1.0
        }
        
        timeObserber = player.addPeriodicTimeObserver(forInterval: interval, queue: .main) { (time) in
            
            let currentTime = Double(time.value) / Double(time.timescale)
            NotificationCenter.default.post(
                name: .audioDidChangeTime,
                object: nil,
                userInfo: ["currentTime": currentTime]
            )
            self.currentTime = currentTime
        }
    }
    
    @objc
    private func playerDidFinishTrackPlaying() {
        currentTime = 0
        self.next()
    }
    
    private  func killTimeObserver() {
        _playerRateObserver?.invalidate()
        if let observer = timeObserber {
            player.removeTimeObserver(observer)
        }
    }
    
    func playAudio(from position: Int) {
        if currentAudio != nil { killTimeObserver() }
        let previous = index
        index = position
        currentTime = 0
        currentAudio = _playlist[index]
        
        let playerItem = AVPlayerItem(url: URL(fileURLWithPath: currentAudio!.path))
        player = AVPlayer(playerItem: playerItem)
        
        player.play()
        isPlaying = true
        addTimeObeserver()

        let thumbnail = _playlist[index].thumbnail
		CommandCenter.defaultCenter.setNowPlayingInfo(
            artworkImage: thumbnail,
            currentTime: currentTime,
            rate: player?.rate ?? 0.0
        )
        
        NotificationCenter.default.post(
            name: .willPlayAudio,
            object: nil,
            userInfo: ["atIndex": index, "previous": previous]
        )
    }
	
	func play() {
		if let player = player {
            player.play()
            return CommandCenter.defaultCenter.setNowPlayingInfo(
                artworkImage: currentAudio?.thumbnail,
                currentTime: currentTime,
                rate: player.rate
            )
        }
        
        playAudio(from: max(0, index))
	}
    
    func previous() {
        var nextPosition = index - 1
        if nextPosition < 0 {
            nextPosition = playlist.count - 1
        }
        playAudio(from: nextPosition)
    }
	
	func next() {
        var nextPosition = index + 1
        if nextPosition > (playlist.count - 1) {
            nextPosition = 0
        }
        playAudio(from: nextPosition)
	}
    
    func togglePlaying() {
        isPlaying ? pause() : play()
    }
    
    func pause() {
        if let player = player {
            player.pause()
        }
        CommandCenter.defaultCenter.setNowPlayingInfo(
            artworkImage: currentAudio?.thumbnail,
            currentTime: currentTime,
            rate: player?.rate ?? 0.0
        )
    }
    
    func getCurrentTime() -> Double {
        return player?.currentTime().seconds ?? 0
    }
	
    func kill() {
		if let player = player {
            killTimeObserver()
            player.replaceCurrentItem(with: nil)
            currentAudio = nil
        }
    }
    
    func setPlayList(_ playList: [AudioModel]) {
        _playlist = playList
        index = -1
    }
    
    func seekTo(_ time: TimeInterval) {
        let seconds = CMTimeMakeWithSeconds(time, preferredTimescale: 1000)
        player.seek(to: seconds)
        currentTime = seconds.seconds
        CommandCenter.defaultCenter.setNowPlayingInfo(
            artworkImage: currentAudio?.thumbnail,
            currentTime: currentTime,
            rate: player?.rate ?? 0.0
        )
    }
}
