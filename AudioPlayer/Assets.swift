// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  public typealias AssetColorTypeAlias = NSColor
  public typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  public typealias AssetColorTypeAlias = UIColor
  public typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

typealias AssetImageType = KeyPath<Asset.Images, ImageAsset>
typealias AssetColorType = KeyPath<Asset.Colors, ColorAsset>

// MARK: - Asset Catalogs


// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public class Asset {
  public class Colors {
    public static let instance: Colors = Colors()
    public let audioBackground = ColorAsset(name: "audioBackground")
    public static var audioBackground: ColorAsset { return instance.audioBackground }
    public let audioIcon = ColorAsset(name: "audioIcon")
    public static var audioIcon: ColorAsset { return instance.audioIcon }
    public let audioIconBackground = ColorAsset(name: "audioIconBackground")
    public static var audioIconBackground: ColorAsset { return instance.audioIconBackground }
    public let audioPlaceholderIcon = ColorAsset(name: "audioPlaceholderIcon")
    public static var audioPlaceholderIcon: ColorAsset { return instance.audioPlaceholderIcon }
    public let audioPrimaryText = ColorAsset(name: "audioPrimaryText")
    public static var audioPrimaryText: ColorAsset { return instance.audioPrimaryText }
    public let audioSecondaryText = ColorAsset(name: "audioSecondaryText")
    public static var audioSecondaryText: ColorAsset { return instance.audioSecondaryText }
    public let cellHighlight = ColorAsset(name: "cellHighlight")
    public static var cellHighlight: ColorAsset { return instance.cellHighlight }
    public let miniPlayerTint = ColorAsset(name: "miniPlayerTint")
    public static var miniPlayerTint: ColorAsset { return instance.miniPlayerTint }
    public let miniplayerBackground = ColorAsset(name: "miniplayerBackground")
    public static var miniplayerBackground: ColorAsset { return instance.miniplayerBackground }
    public let miniplayerShadow = ColorAsset(name: "miniplayerShadow")
    public static var miniplayerShadow: ColorAsset { return instance.miniplayerShadow }
    public let miniplayerText = ColorAsset(name: "miniplayerText")
    public static var miniplayerText: ColorAsset { return instance.miniplayerText }
    public let seekBarPrimary = ColorAsset(name: "seekBarPrimary")
    public static var seekBarPrimary: ColorAsset { return instance.seekBarPrimary }
    public let seekBarSecondary = ColorAsset(name: "seekBarSecondary")
    public static var seekBarSecondary: ColorAsset { return instance.seekBarSecondary }
  }
  public class Images {
    public static let instance: Images = Images()
    public let artPlaceholder = ImageAsset(name: "ArtPlaceholder")
    public static var artPlaceholder: ImageAsset { return instance.artPlaceholder }
    public let icAudio = ImageAsset(name: "ic_audio")
    public static var icAudio: ImageAsset { return instance.icAudio }
    public let icCheck = ImageAsset(name: "ic_check")
    public static var icCheck: ImageAsset { return instance.icCheck }
    public let icForward30 = ImageAsset(name: "ic_forward_30")
    public static var icForward30: ImageAsset { return instance.icForward30 }
    public let icNext = ImageAsset(name: "ic_next")
    public static var icNext: ImageAsset { return instance.icNext }
    public let icPause = ImageAsset(name: "ic_pause")
    public static var icPause: ImageAsset { return instance.icPause }
    public let icPlay = ImageAsset(name: "ic_play")
    public static var icPlay: ImageAsset { return instance.icPlay }
    public let icPrevious = ImageAsset(name: "ic_previous")
    public static var icPrevious: ImageAsset { return instance.icPrevious }
    public let icReplay10 = ImageAsset(name: "ic_replay_10")
    public static var icReplay10: ImageAsset { return instance.icReplay10 }
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension UITextField {
  @discardableResult
  func setTextColor(_ color: AssetColorType) -> Self {
    self.textColor = Asset.Colors.instance[keyPath: color].color
    return self
  }
}

// MARK: Extensions
extension UIButton {
  @discardableResult
  func setImage(_ keyPath: AssetImageType, for state: UIControl.State) -> Self {
    setImage(Asset.Images.instance[keyPath: keyPath].image, for: state)
    return self
  }

  @discardableResult
  func setImage(_ image: AssetImageType, tint: AssetColorType, for state: UIControl.State) -> Self {
    let color = Asset.Colors.instance[keyPath: tint].color
    setImage(Asset.Images.instance[keyPath: image].image.tintWith(color: color), for: state)
    return self
  }
  @discardableResult
  func setImage(_ image: AssetImageType, tint: AssetColorType, alpha: CGFloat, for state: UIControl.State) -> Self {
    let color = Asset.Colors.instance[keyPath: tint].color.withAlphaComponent(alpha)
    setImage(Asset.Images.instance[keyPath: image].image.tintWith(color: color), for: state)
    return self
  }
  @discardableResult
  func setBackgroundImage(_ image: AssetImageType, tint: AssetColorType, for state: UIControl.State) -> Self {
    let color = Asset.Colors.instance[keyPath: tint].color
    setBackgroundImage(Asset.Images.instance[keyPath: image].image.tintWith(color: color), for: state)
    return self
  }

  @available(*, deprecated, message: "Use setBackgroundColor() instead")
  func setBackgroundImage(_ color: AssetColorType, for state: UIControl.State) {
    setBackgroundImage(UIImage.with(color: color), for: state)
  }

  @available(*, deprecated, message: "Use setBackgroundColor() instead")
  func setBackgroundImage(_ color: AssetColorType, alpha: CGFloat, for state: UIControl.State) {
    setBackgroundImage(UIImage.with(color: Asset.Colors.instance[keyPath: color].color.withAlphaComponent(alpha)), for: state)
  }

  @discardableResult
  func setBackgroundColor(_ color: AssetColorType, for state: UIControl.State) -> Self {
    setBackgroundImage(UIImage.with(color: color), for: state)
    return self
  }

  @discardableResult
  func setBackgroundColor(_ color: AssetColorType, alpha: CGFloat, for state: UIControl.State) -> Self {
    setBackgroundImage(UIImage.with(color: Asset.Colors.instance[keyPath: color].color.withAlphaComponent(alpha)), for: state)
    return self
  }
  @discardableResult
  func setTitleColor(_ color: AssetColorType, for state: UIControl.State) -> Self {
    setTitleColor(Asset.Colors.instance[keyPath: color].color, for: state)
    return self
  }
}

extension UIView {
  var width: CGFloat { frame.width }
  var height: CGFloat { frame.height }
  var origin: CGPoint { frame.origin }
  @objc
  var size: CGSize { frame.size }

  convenience init(background: AssetColorType) {
    self.init()
    setBackgroundColor(background)
  }

  @discardableResult
  func setBorderColor(_ color: AssetColorType) -> Self {
    layer.borderColor = Asset.Colors.instance[keyPath: color].color.cgColor
    return self
  }

  @discardableResult
  func setBackgroundColor(_ color: AssetColorType) -> Self {
    self.backgroundColor = Asset.Colors.instance[keyPath: color].color
    return self
  }

  @discardableResult
  func setBackgroundColor(_ color: AssetColorType, alpha: CGFloat) -> Self {
    self.backgroundColor = Asset.Colors.instance[keyPath: color].color.withAlphaComponent(alpha)
    return self
  }
}

extension UIImage {
  convenience init!(image: AssetImageType) {
    self.init(named: Asset.Images.instance[keyPath: image].name)
  }

  static func create(image: AssetImageType, tint: AssetColorType) -> UIImage? {
    let image = UIImage(image: image)?.tintWith(color: Asset.Colors.instance[keyPath: tint].color)
    return image
  }

  class func with(color: AssetColorType, alpha: CGFloat = 1.0) -> UIImage? {
    return with(color: Asset.Colors.instance[keyPath: color].color)
  }
  class func with(color: UIColor, alpha: CGFloat = 1.0) -> UIImage? {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size)
    if let context = UIGraphicsGetCurrentContext() {
      context.setFillColor(color.withAlphaComponent(alpha).cgColor)
      context.fill(rect)
    }
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
  }

  func tintWith(color: UIColor) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
    color.setFill()

    let context = UIGraphicsGetCurrentContext()
    context?.translateBy(x: 0, y: self.size.height)
    context?.scaleBy(x: 1.0, y: -1.0)
    context?.setBlendMode(CGBlendMode.normal)

    let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
    context?.clip(to: rect, mask: self.cgImage!)
    context?.fill(rect)

    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return newImage!
  }
}

extension UIImageView {
  convenience init(image: AssetImageType) {
    self.init(image: Asset.Images.instance[keyPath: image].image)
  }

  convenience init(image: AssetImageType, tint: AssetColorType) {
    self.init(image: Asset.Images.instance[keyPath: image].image)
    self.setTintColor(tint)
  }

  @discardableResult
  func setImage(_ keyPath: AssetImageType) -> Self {
    self.image = Asset.Images.instance[keyPath: keyPath].image
    return self
  }

  @discardableResult
  func setImage(_ image: AssetImageType, tint: AssetColorType) -> Self {
    self.image = Asset.Images.instance[keyPath: image].image
    setTintColor(tint)
    return self
  }

  @discardableResult
  func setTintColor(_ tint: AssetColorType) -> Self {
    self.tintColor = Asset.Colors.instance[keyPath: tint].color
    return self
  }
}

extension UITableView {
  @discardableResult
  func setSeparator(_ color: AssetColorType) -> Self {
    self.separatorColor = Asset.Colors.instance[keyPath: color].color
    return self
  }
}

extension UILabel {
  convenience init(tint: AssetColorType) {
    self.init()
    self.setTextColor(tint)
  }
  @discardableResult
  func setTextColor(_ color: AssetColorType) -> Self {
    self.textColor = Asset.Colors.instance[keyPath: color].color
    return self
  }
}

extension CAShapeLayer {
  func setFillColor(_ color: AssetColorType) {
    fillColor = Asset.Colors.instance[keyPath: color].color.cgColor
  }
}

// MARK: UIColor
public extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    convenience init(all: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(all) / 255.0, green: CGFloat(all) / 255.0, blue: CGFloat(all) / 255.0, alpha: alpha)
    }
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

// MARK: - Implementation Details

public struct ColorAsset {
  public fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  public var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

public extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

public struct DataAsset {
  public fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  public var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
public extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

public struct ImageAsset {
  public fileprivate(set) var name: String

  public var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

public extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
