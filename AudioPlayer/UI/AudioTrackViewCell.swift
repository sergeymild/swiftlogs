//
//  AudioTrackViewCell.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import PinLayout
import UIKit

class AudioTrackViewCell: UICollectionViewCell {
    let indicator: UIImageView = {
        let view = UIImageView(image: \.icCheck, tint: \.audioPlaceholderIcon)
        view.contentMode = .scaleToFill
        return view
    }()
    
    let coverImageView: CoverImageView = {
        let imageView = CoverImageView()
        imageView.coverIconView.setImage(\.icAudio)
        imageView.coverIconView.setTintColor(\.audioPlaceholderIcon)
        imageView.coverBackgroundView.setBackgroundColor(\.audioIconBackground)
        imageView.cornerRadius = 6
        return imageView
    }()
    
    let verticallyCenteredView = UIView()
    
    let titleLabel: UILabel = {
        let label = UILabel(tint: \.audioPrimaryText)
        label.font =  UIFont.systemFont(ofSize: 14, weight: .bold)
        label.numberOfLines = 2
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel(tint: \.audioSecondaryText)
        label.font =  UIFont.systemFont(ofSize: 12, weight: .medium)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(coverImageView)
        addSubview(indicator)
        
        addSubview(verticallyCenteredView)
        verticallyCenteredView.addSubview(titleLabel)
        verticallyCenteredView.addSubview(subtitleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        let left = pin.safeArea.left
        let right = pin.safeArea.right
        coverImageView.pin
            .left(left == 0 ? 16 : left)
            .vCenter()
            .size(42)
        
        indicator.pin
            .vCenter()
            .right(right == 0 ? 16 : right)
            .size(28)
        
        titleLabel.pin
            .left()
            .right()
            .top()
            .sizeToFit(.width)
        
        subtitleLabel.pin
            .left()
            .right()
            .below(of: titleLabel)
            .marginTop(4)
            .sizeToFit(.width)
        
        verticallyCenteredView.pin
            .after(of: coverImageView)
            .marginLeft(8)
            .before(of: indicator)
            .vCenter()
            .wrapContent(.vertically)
    }
    
    func bind(_ model: AudioModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        layoutSubviews()
        indicator.isHidden = true
        AudioPlayerKingfisherWrapper.handle(
            path: model.path,
            width: 42 * 2,
            height: 42 * 2,
            imageView: coverImageView.coverImage
        )
    }
    
    func set(selected: Bool) {
        indicator.isHidden = !selected
    }
}
