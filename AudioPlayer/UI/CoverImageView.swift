//
//  CoverImageView.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 06/03/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import PinLayout
import UIKit

class CoverImageView: UIView {
    var cornerRadius: CGFloat = 0.5 {
        didSet {
            coverImage.layer.cornerRadius = cornerRadius
            coverBackgroundView.layer.cornerRadius = cornerRadius
        }
    }
    
    let coverImage: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let coverBackgroundView = UIView()
    let coverIconView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(coverBackgroundView)
        addSubview(coverIconView)
        addSubview(coverImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        coverBackgroundView.pin.all()
        if coverIconView.image != nil {
            coverIconView.pin.center().width(width * 0.5).aspectRatio()
            coverImage.pin.all()
        }
    }
}
