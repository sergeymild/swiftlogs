//
//  AudioPlayerViewController.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation
import UIKit
import Logs

public class AudioPlayerViewController: UIViewController {
    var lastPlayedAudioIndex: Int = -2
    private let playerIndexPath = IndexPath(item: 0, section: 0)
    private weak var miniplayerParentView: UIView?
    
    private let playerView = AudioPlayerView()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.contentInset = .zero
        view.showsVerticalScrollIndicator = false
        view.dataSource = self
        view.delegate = self
        view.backgroundColor = .clear
        view.allowsMultipleSelection = false
        view.register(AudioPlayerViewCellHolder.self)
        view.register(AudioTrackViewCell.self)
        return view
    }()
    
    public override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    public init(
        playlist: [AudioModel],
        startIndex: Int = 1,
        restartPlay: Bool = true,
        miniplayerParentView: UIView? = nil
    ) {
        self.miniplayerParentView = miniplayerParentView
        super.init(nibName: nil, bundle: nil)
        
        if restartPlay {
            AudioPlayer.shared.setPlayList(playlist)
            AudioPlayer.shared.playAudio(from: startIndex)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        presentationController?.delegate = self
        view.setBackgroundColor(\.audioBackground)
        view.addSubview(collectionView)
        collectionView.reloadData()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(willPlayAudio),
            name: .willPlayAudio,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(audioDidChangeTime),
            name: .audioDidChangeTime,
            object: nil
        )
    }
    
    override public func viewWillLayoutSubviews() {
        collectionView.pin.all()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .willPlayAudio, object: nil)
        NotificationCenter.default.removeObserver(self, name: .audioDidChangeTime, object: nil)
        deinitLog(self)
    }
}

extension AudioPlayerViewController: UICollectionViewDataSource {
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return section == 0
            ? 1
            : AudioPlayer.shared.playlist.count
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeue(
                AudioPlayerViewCellHolder.self, for: indexPath)
            playerView.bind(AudioPlayer.shared.currentAudio)
            cell.bind(playerView: playerView)
            return cell
        }
        
        let cell = collectionView.dequeue(
            AudioTrackViewCell.self, for: indexPath)
        let model = AudioPlayer.shared.playlist[indexPath.row]
        cell.bind(model)
        cell.set(selected: indexPath.row == AudioPlayer.shared.index)
        return cell
    }
}

extension AudioPlayerViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        if indexPath.section == 0 {
            let top = view.safeAreaLayoutGuide.layoutFrame.minY - 44
            return .init(width: view.frame.width, height: view.frame.height - top)
        }
        
        return .init(width: collectionView.frame.width, height: 72)
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        if section == 0 {
            return .init(
                top: view.safeAreaLayoutGuide.layoutFrame.minY,
                left: 0,
                bottom: 0,
                right: 0
            )
        }
        return .zero
    }
    
    public func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if indexPath.section == 0 { return }
        collectionView.highlight(at: indexPath, alpha: 0.1)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if indexPath.section == 0 { return }
        collectionView.unhighlight(at: indexPath, color: \.audioBackground)
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        if indexPath.section == 0 { return }
        let current = AudioPlayer.shared.index
        if current == indexPath.row { return }
        AudioPlayer.shared.playAudio(from: indexPath.row)
    }
}

extension AudioPlayerViewController: UIAdaptivePresentationControllerDelegate {
    public func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        guard let window = miniplayerParentView else {
            return AudioPlayer.shared.kill()
        }
        showMiniPlayer(window: window)
    }
}

extension AudioPlayerViewController {
    @objc
    private func willPlayAudio(notification: Notification) {
        guard let position = notification.userInfo?["atIndex"] as? Int,
            let previous = notification.userInfo?["previous"] as? Int
            else {
            return
        }
        
        
        UIView.setAnimationsEnabled(false)
        collectionView.performBatchUpdates({
            var reload: [IndexPath] = []
            reload.append(IndexPath.init(row: 0, section: 0))
            if position >= 0 {
                reload.append(IndexPath.init(row: position, section: 1))
            }
            if previous >= 0 {
                reload.append(IndexPath.init(row: previous, section: 1))
            }
            collectionView.reloadItems(at: reload)
        }, completion: { _ in
            UIView.setAnimationsEnabled(true)
        })
        //playerView?.bind(AudioPlayer.shared.currentAudio)
    }
    
    @objc
    private func audioDidChangeTime(notification: Notification) {
        guard let time = notification.userInfo?["currentTime"] as? Double else {
            return
        }
        guard let model = AudioPlayer.shared.currentAudio else {
            return
        }
        playerView.audioDidChangeTime(model: model, time: time)
    }
}
