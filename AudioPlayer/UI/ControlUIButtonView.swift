//
//  ControlUIButtonView.swift
//  FileManager
//
//  Created by Sergei on 03/05/2018.
//  Copyright © 2018 Sergey Golishnikov. All rights reserved.
//

import UIKit

public class ControlUIButtonView: UIView {
    
    public var didTouchEnd: (() -> Void)? = nil
    
    var isEndBeganAnimation = true
    var isTouchEnd = true
    
    let iconImageView: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconImageView)
    }
    
    public override func layoutSubviews() {
        iconImageView.pin.all()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouchEnd = false
        isEndBeganAnimation = false
        UIView.animate(withDuration: 0.15, animations: {
            self.iconImageView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }, completion: { _ in
            if self.isTouchEnd {
                self.scaleToNormal()
            }
            self.isEndBeganAnimation = true
        })
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isEndBeganAnimation {
            scaleToNormal()
        }
        isTouchEnd = true
        didTouchEnd?()
    }
    
    fileprivate func scaleToNormal() {
        UIView.animate(withDuration: 0.15, animations: {
            self.iconImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { _ in
            self.isEndBeganAnimation = true
        })
    }
}
