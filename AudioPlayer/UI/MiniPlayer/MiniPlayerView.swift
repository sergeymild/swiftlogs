//
//  MiniPlayerView.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit
import CommonExtensions
import Logs

public func dismissMiniPlayerView(window: UIView?) {
    guard let window = window else { return }
    let miniPlayer = window.subviews.first(where: { $0 is MiniPlayerView }) as? MiniPlayerView
    miniPlayer?.didViewDismiss()
}

public func showMiniPlayer(window: UIView?) {
    guard let window = window else { return }
    let viewHeight: CGFloat = 72
    if window.subviews.contains(where: { $0 is MiniPlayerView }) { return }
    let mini = MiniPlayerView()
    window.addSubview(mini)
    window.bringSubviewToFront(mini)
    mini.translatesAutoresizingMaskIntoConstraints = false
    mini.leftAnchor.constraint(equalTo: window.leftAnchor, constant: 16).isActive = true
    mini.bottomAnchor.constraint(equalTo: window.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    mini.rightAnchor.constraint(equalTo: window.rightAnchor, constant: -16).isActive = true
    mini.heightAnchor.constraint(equalToConstant: viewHeight).isActive = true
    mini.transform = CGAffineTransform(translationX: viewHeight, y: 0)
    
    UIView.animate(withDuration: 0.25) {
        mini.transform = CGAffineTransform(translationX: 0, y: 0)
    }
}

class MiniPlayerView: SwipeToDismissView {
    let titleLabel: MarqueeLabel = {
        let label = MarqueeLabel(tint: \.miniplayerText)
        label.animationDelay = 3
        label.fadeLength = 10
        label.trailingBuffer = 10
        label.numberOfLines = 0
        label.isUserInteractionEnabled = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    let playPauseButton: PlayPauseButtonView = {
        let button = PlayPauseButtonView()
        button.iconImageView.setTintColor(\.miniPlayerTint)
        button.didTouchEnd = AudioPlayer.shared.togglePlaying
        return button
    }()
    
    let previousButton: ControlUIButtonView = {
        let button = ControlUIButtonView()
        button.iconImageView.setImage(\.icPrevious)
        button.iconImageView.setTintColor(\.miniPlayerTint)
        button.didTouchEnd = AudioPlayer.shared.previous
        return button
    }()
    
    let nextButton: ControlUIButtonView = {
        let button = ControlUIButtonView()
        button.iconImageView.setImage(\.icNext)
        button.iconImageView.setTintColor(\.miniPlayerTint)
        button.didTouchEnd = AudioPlayer.shared.next
        return button
    }()
    
    let containerView: UIView = {
        let view = UIView()
        // set the cornerRadius of the containerView's layer
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        view.setBackgroundColor(\.miniplayerBackground)
        return view
    }()
    
    let openMainAudioPlayerButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        clipsToBounds = true
        containerView.addSubview(openMainAudioPlayerButton)
        containerView.addSubview(titleLabel)
        containerView.addSubview(playPauseButton)
        containerView.addSubview(previousButton)
        containerView.addSubview(nextButton)
        
        // set the shadow of the view's layer
        shadow(
            color: Asset.Colors.miniplayerShadow.color,
            opacity: 0.4,
            radius: 10.0,
            width: 2,
            height: 2
        )
        
        addSubview(containerView)
        
        // add constraints
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        // pin the containerView to the edges to the view
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

        playPauseButton.isPlaying = AudioPlayer.shared.isPlaying
        
        self.titleLabel.text =
            AudioPlayer.shared.currentAudio?.title ?? "Title will be here"
        
        openMainAudioPlayerButton.addTarget(self, action: #selector(showMainPlayerClick), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(willPlayAudio),
            name: .willPlayAudio,
            object: nil
        )
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        shadow(
            color: Asset.Colors.miniplayerShadow.color,
            opacity: 0.4,
            radius: 10.0,
            width: 2,
            height: 2
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func showMainPlayerClick() {
        NotificationCenter.default.post(name: .didShowMainPlayerClick, object: nil)
    }
    
    @objc
    private func willPlayAudio(notification: Notification) {
        guard let model = AudioPlayer.shared.currentAudio else { return }
        titleLabel.text = model.title
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playPauseButton.pin.size(42).vCenter().right(8)
        nextButton.pin.size(42).vCenter().before(of: playPauseButton).marginRight(8)
        previousButton.pin.size(42).vCenter().before(of: nextButton).marginRight(8)
        titleLabel.pin.left(8).height(42).vCenter().before(of: previousButton)
        openMainAudioPlayerButton.pin.left().top().right().bottom()
    }
    
    override func didViewDismiss() {
        removeFromSuperview()
        AudioPlayer.shared.kill()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .willPlayAudio, object: nil)
        NotificationCenter.default.removeObserver(self, name: .audioDidChangeTime, object: nil)
        deinitLog(self)
    }
}
