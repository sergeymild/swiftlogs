//
//  UICollectionView+Extensions.swift
//  CommonExtensions
//
//  Created by Sergei Golishnikov on 26/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit


extension UICollectionView {
    func highlight(at: IndexPath, alpha: CGFloat = 1.0) {
        cellForItem(at: at)?.setBackgroundColor(\.cellHighlight, alpha: alpha)
    }
    
    func unhighlight(at: IndexPath, color: AssetColorType) {
        cellForItem(at: at)?.setBackgroundColor(color)
    }
    
    func register(_ cell: UICollectionViewCell.Type) {
        let identifier = String(describing: cell)
        self.register(cell, forCellWithReuseIdentifier: identifier)
    }
    
    func dequeue<T>(_ cell: T.Type, for indexPath: IndexPath) -> T where T: UICollectionViewCell {
        let identifier = String(describing: cell)
        return dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! T
    }
}
