//
//  AudioPlayer.h
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AudioPlayer.
FOUNDATION_EXPORT double AudioPlayerVersionNumber;

//! Project version string for AudioPlayer.
FOUNDATION_EXPORT const unsigned char AudioPlayerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AudioPlayer/PublicHeader.h>


