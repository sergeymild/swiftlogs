//
//  YandexOAuth.swift
//  Corie
//
//  Created by Sergei Golishnikov on 28/12/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation
import SafariServices
import UIKit
import WebKit

public struct YandexAccessToken {
    let token: String
    let uid: String
}

public class YandexOAuth {
    let kCSERFKey = "kCSERFKeyYandexDisk"
    var urls: [String: URL] = [:]
    
    public static let shared = YandexOAuth()
    
    public private(set) var authorizedClient: String?
    
    private weak var controller: UIViewController?
    
    private init() {
        urls["redirect"] = URL(string: "yd-l14w2ou3wqem://success")!
        
        if let token = getFirstAccessToken() {
            authorizedClient = token.token
        }
    }
    
    public var client: YandexDisk? {
        return authorizedClient == nil ? nil : YandexDisk(token: authorizedClient!)
    }
    
    /// Unlink the user.
    public func unlinkClients() {
        authorizedClient = nil
        let _ = YandexKeychain.clear()
    }
    
    private func trunc(text: String, length: Int) -> String {
      return (text.count > length) ? String(text.prefix(length)) : text
    }
    
    private func createAuthURL() -> URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "oauth.yandex.ru"
        components.path = "/authorize"

        let state = UserDefaults.standard.string(forKey: kCSERFKey) ?? trunc(text: UUID.init().uuidString.replacingOccurrences(of: "-", with: ""), length: 50)
        UserDefaults.standard.setValue(state, forKey: kCSERFKey)

        components.queryItems = [
            URLQueryItem(name: "response_type", value: "token"),
            URLQueryItem(name: "client_id", value: "736bcd54041e437cab4bb34b97727dd5"),
            URLQueryItem(name: "redirect_uri", value: urls["redirect"]!.absoluteString),
            URLQueryItem(name: "device_id", value: "yd-l14w2ou3wqem"),
            URLQueryItem(name: "device_name", value: "Iphone"),
            URLQueryItem(name: "force_confirm", value: "true"),
            URLQueryItem(name: "state", value: state),
            URLQueryItem(name: "display", value: "popup"),
        ]
        let url = components.url!
        debugPrint(url)
        return url
    }
    
    public func presentAuthChannel(
        sharedApplication: UIApplication,
        viewController: UIViewController
    ) {
        self.controller = viewController
        let cancelHandler: (() -> Void) = {
            let cancelUrl = URL(string: "yd-l14w2ou3wqem://2/cancel")!
            self.openURL(cancelUrl)
        }
        
        
        let safariViewController = MobileSafariViewController(url: createAuthURL(), cancelHandler: cancelHandler)
        viewController.present(safariViewController, animated: true, completion: nil)
    }
    
    fileprivate func canHandleURL(_ url: URL) -> Bool {
        for known in self.urls.values {
            if url.scheme == known.scheme && url.host == known.host && url.path == known.path {
                return true
            }
        }
        return false
    }
    
    // DO NOT CHANGE THIS IMPLEMENTATION
    @discardableResult
    private func openURL(_ url: URL) -> Bool {
        let sharedAppSelector = NSSelectorFromString("sharedApplication")
        guard
            let classObject = NSClassFromString("UIApplication") as? UIApplication.Type,
            let object = classObject.perform(sharedAppSelector).takeRetainedValue() as? UIApplication
            else { return false }
        return object.perform(NSSelectorFromString("openURL:"), with: url) != nil
    }
    
    public func handleRedirectURL(_ url: URL) -> Bool {
        if (url.host == "1" && url.path == "/cancel") || (url.host == "2" && url.path == "/cancel") {
            return false
        }
        
        if !self.canHandleURL(url) {
            return false
        }
        
        guard let result = extractFromRedirectURL(url) else {
            return false
        }
        
        _ = YandexKeychain.set(result.uid, value: result.token)
        authorizedClient = result.token
        dismissAuthController()
        return true
    }
    
    private func extractFromRedirectURL(_ url: URL) -> YandexAccessToken? {
        var results = [String: String]()
        let pairs  = url.fragment?.components(separatedBy: "&") ?? []

        for pair in pairs {
            let kv = pair.components(separatedBy: "=")
            results.updateValue(kv[1], forKey: kv[0])
        }

        let state = results["state"]
        let storedState = UserDefaults.standard.string(forKey: kCSERFKey)

        if state == nil || storedState == nil || state != storedState {
            debugPrint("Auth flow failed because of inconsistent state.")
            return nil
        } else {
            // reset upon success
            UserDefaults.standard.setValue(nil, forKey: kCSERFKey)
        }
        guard let accessToken = results["access_token"] else {
            return nil
        }
        return .init(token: accessToken, uid: "yd-l14w2ou3wqem")
    }
    
    private func getFirstAccessToken() -> YandexAccessToken? {
        return self.getAllAccessTokens().values.first
    }
    
    private func getAllAccessTokens() -> [String : YandexAccessToken] {
        let users = YandexKeychain.getAll()
        var ret = [String : YandexAccessToken]()
        for user in users {
            if let accessToken = YandexKeychain.get(user) {
                ret[user] = YandexAccessToken(token: accessToken, uid: user)
            }
        }
        return ret
    }
    
    private func dismissAuthController() {
        if let controller = self.controller {
            if let presentedViewController = controller.presentedViewController {
                if presentedViewController.isBeingDismissed == false && presentedViewController is MobileSafariViewController {
                    controller.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}

fileprivate class MobileSafariViewController: SFSafariViewController, SFSafariViewControllerDelegate {
    var cancelHandler: (() -> Void) = {}

    public init(
        url: URL,
        cancelHandler: @escaping (() -> Void)
    ) {
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = false
        super.init(url: url, configuration: config)
        self.cancelHandler = cancelHandler
        self.delegate = self;
    }

    public func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        if (!didLoadSuccessfully) {
            controller.dismiss(animated: true, completion: nil)
        }
    }

    public func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.cancelHandler()
    }
    
}
