//
//  YandexSdk.h
//  YandexSdk
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for YandexSdk.
FOUNDATION_EXPORT double YandexSdkVersionNumber;

//! Project version string for YandexSdk.
FOUNDATION_EXPORT const unsigned char YandexSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YandexSdk/PublicHeader.h>


