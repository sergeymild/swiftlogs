//
//  Logs.swift
//  Logs
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation

fileprivate let dateFormatter = DateFormatter()

public func deinitLog(_ any: Any) {
    #if DEBUG
    debugPrint("<<< DEINIT: \(String(describing: type(of: any)))")
    #endif
}

public func logMessage(_ message: Any? = nil, file: String = #file, function: String = #function) {
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let className = file.split(separator: "/").last?.replacingOccurrences(of: ".swift", with: "") ?? ""
    let functionName = function.split(separator: "(").first ?? ""
    let tag = "LOG: [\(dateFormatter.string(from: Date()))]"
    guard let message = message else {
        debugPrint("\(tag): \(className).\(functionName)()")
        return
    }
    
    debugPrint("\(tag): \(className).\(functionName)(\(String(describing: message)))")
}
