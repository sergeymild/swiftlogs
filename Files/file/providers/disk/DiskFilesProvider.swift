//
//  DiskFilesProvider.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 20/02/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation

public class DiskFilesProvider: FolderProvider {
    fileprivate let byteCountFormatter: ByteCountFormatter
    fileprivate let fileNativeWrapper: FileNativeWrapper
    fileprivate let mediaRetriever: MediaTypeRetriever
    
    let rootFolder = ""
    
    var showFileExtension: Bool {
        UserDefaults.standard.bool(forKey: "showFileExtension")
    }
    
    public init(
        byteCountFormatter: ByteCountFormatter = ByteCountFormatter(),
        fileNativeWrapper: FileNativeWrapper = FileNativeWrapper(),
        mediaRetriever: MediaTypeRetriever = FileMediaTypeRetriever()
    ) {
        self.byteCountFormatter = byteCountFormatter
        self.fileNativeWrapper = fileNativeWrapper
        self.mediaRetriever = mediaRetriever
    }
    
    public func createAbsolutePath(rootPath: String, path: String) -> String {
        if path.isEmpty {
            return "\(rootPath)"
        }
        return "\(rootPath)/\(path.deletingPrefix("/"))"
    }
    
    public func removeDocumentsAndRootFolders(rootPath: String, path: String) -> String {
        return path.replacingOccurrences(of: "\(rootPath)/", with: "")
    }
    
    var _documentsPath: String?
    public func rootPath() -> String {
        if let path = _documentsPath { return path }
        _documentsPath = documentsPath
        return _documentsPath!
    }
    
    public func getFolders(parent: String) -> [Folder] {
        let path = removeDocumentsAndRootFolders(rootPath: rootPath(), path: parent)
        let absolutePath = createAbsolutePath(rootPath: rootPath(), path: path)
        
        let folders = fileNativeWrapper.getDirectories(absolutePath).map {
            return Folder(
                name: $0.name,
                absolutePath: $0.absolutePath,
                isHasSubfolders: fileNativeWrapper.isDirectoryContainsDirectories($0.absolutePath))
        }

        return folders
    }
    
    @discardableResult
    public func getFilesList(path: String) -> (SortType, [File]) {
        let absolutePath = concatWithDocumentsIfNeed(path: path)
        
        let sortType = getSortType(folderPath: absolutePath)
        
        let resources = self.fileNativeWrapper.getFilesList(absolutePath, skipHidden: true, sort: sortType.rawValue).map {
            self.create(from: $0, showFileExtension: showFileExtension)
        }

        return (sortType, resources)
    }
    
    public func getAllFromFolder(path: String) -> [String] {
        let absolutePath = concatWithDocumentsIfNeed(path: path)
        let resources = self.fileNativeWrapper.getFilesList(absolutePath, skipHidden: true, sort: SortType.name_asc.rawValue)
        return resources.map { $0.absolutePath }
    }
    
    public func getAllDirectoryFiles(path: String, sortType: SortType) -> [String] {
        let absolutePath = concatWithDocumentsIfNeed(path: path)
        let resources = self.fileNativeWrapper.getFilesList(absolutePath, skipHidden: true, sort: sortType.rawValue)
        return resources.map { $0.absolutePath }
    }
    
    public func move(file: File, parent: String) -> Bool {
        var name = file.name
        if !file.ext.isEmpty && !name.contains(file.ext) {
            name = "\(name).\(file.ext)"
        }
        return fileNativeWrapper.moveFile(
            concatWithDocumentsIfNeed(path: file.path),
            to: "\(concatWithDocumentsIfNeed(path: parent))/\(name)")
    }
    
    public func move(file: String, parent: String) -> Bool {
        var name = file.nameWithExtension
        if !file.ext.isEmpty && !name.contains(file.ext) {
            name = "\(name).\(file.ext)"
        }
        return fileNativeWrapper.moveFile(
            concatWithDocumentsIfNeed(path: file),
            to: "\(concatWithDocumentsIfNeed(path: parent))/\(name)")
    }
    
    public func getFile(path: String) -> File {
        let entry = fileNativeWrapper.getFile(createAbsolutePath(rootPath: rootPath(), path: path))
        return create(from: entry, showFileExtension: showFileExtension)
    }
    
    @discardableResult
    public func remove(path: String) -> Bool {
        return fileNativeWrapper.remove(path)
    }
    
    public func rename(oldFile: File, newName: String, parentPath: String) -> File {
        var name = newName
        if !oldFile.ext.isEmpty {
            name = "\(name).\(oldFile.ext)"
        }

        let newPath = concatWithDocumentsIfNeed(path: "\(parentPath)/\(name)")
        let _ = fileNativeWrapper.rename(
            concatWithDocumentsIfNeed(path: oldFile.path),
            to: newPath)
        oldFile.name = name
        oldFile.path = removeDocumentsPathIfNeed(path: newPath)
        return oldFile
    }
    
    public func rename(oldFile: String, newName: String, parentPath: String) -> (Bool, String) {
        var name = newName
        if !oldFile.ext.isEmpty {
            name = "\(name).\(oldFile.ext)"
        }

        let newPath = concatWithDocumentsIfNeed(path: "\(parentPath)/\(name)")
        let isRenamed = fileNativeWrapper.rename(
            concatWithDocumentsIfNeed(path: oldFile),
            to: newPath)
        return (isRenamed, newPath)
    }
    
    public func remove(file: File) -> Bool {
        if file.fileType == .directory {
            return fileNativeWrapper.remove(
            concatWithDocumentsIfNeed(path: file.path))
        }
        return fileNativeWrapper.removeFile(
        concatWithDocumentsIfNeed(path: file.path))
    }
    
    @discardableResult
    public func remove(file: String) -> Bool {
        return fileNativeWrapper.removeFile(
        concatWithDocumentsIfNeed(path: file))
    }
    
    public func saveSort(type: SortType, for folder: String) {
        let sortFile = "\(concatWithDocumentsIfNeed(path: folder))/.folder_sort"
        fileNativeWrapper.write(sortFile, text: type.rawValue)
    }
    
    public func getSortType(folderPath: String) -> SortType {
        let sortFile = "\(folderPath)/.folder_sort"
        if !fileNativeWrapper.isExists(sortFile) {
            fileNativeWrapper.write(sortFile, text: SortType.name_asc.rawValue)
            return .name_asc
        }
        let sortText = fileNativeWrapper.read(sortFile)
        if let type = SortType.init(rawValue: sortText) {
            return type
        }
        return .name_asc
    }
    
    @discardableResult
    public func createFolder(name: String, parent: String) -> File {
        let parentPath = concatWithDocumentsIfNeed(path: parent)
        var newFolderPath = "\(parentPath)/\(name)"
        var suffix = 1
        
        while fileNativeWrapper.isDirectoryExists(newFolderPath) {
            newFolderPath = "\(parentPath)/\(name)-\(suffix)"
            suffix += 1
        }
        
        fileNativeWrapper.createDirectory(newFolderPath)
        let file = create(
            from: fileNativeWrapper.getFile(newFolderPath),
            showFileExtension: showFileExtension
        )
        return file
    }
    
    @discardableResult
    public func writeFileAttribute(path: String, name: String, value: String) -> Bool {
        var path = removeDocumentsAndRootFolders(rootPath: rootPath(), path: path)
        path = createAbsolutePath(rootPath: rootPath(), path: path)
        return fileNativeWrapper.writeFileAttribute(path, name: name, value: value)
    }
    
    public func readFileAttribute(path: String, name: String) -> String {
        var path = removeDocumentsAndRootFolders(rootPath: rootPath(), path: path)
        path = createAbsolutePath(rootPath: rootPath(), path: path)
        return fileNativeWrapper.readFileAttribute(path, name: name)
    }
    
    public func directoryInfo(path: String) -> DirectoryInfoEntry {
        return fileNativeWrapper.directoryInfo(concatWithDocumentsIfNeed(path: path), includeHidden: false)
    }
    
    fileprivate func create(from entry: FileEntry, showFileExtension: Bool) -> File {
        let path = removeDocumentsAndRootFolders(rootPath: rootPath(), path: entry.absolutePath)
        let ext = NSString(string: entry.absolutePath).pathExtension
        let type = FileTypeProvider.getFileType(entry: entry)
        var filesCount: UInt64 = 0
        var entrySize: UInt64 = 0
        if entry.isDirectory {
            let info = directoryInfo(path: entry.absolutePath)
            filesCount = info.filesCount
            entrySize = info.size
        } else {
            filesCount = 0
            entrySize = entry.size
        }
        
        
        
        var name = showFileExtension ? entry.name : entry.name.withoutExtension
        var formattedDuration = ""
        var mediaInformation: MediaInformation?
        if type == .audio || type == .video {
            let information = mediaRetriever.retrieveMediaInformation(path: entry.absolutePath, type: type)
            formattedDuration = information.formattedDuration
            mediaInformation = information
        }

        name = name.replacingOccurrences(of: "–", with: "-")

        let resource = File(
            name: name,
            path: path,
            lastModificationTime: entry.modificationDate ?? Date(),
            size: Int64(entrySize),
            fileType: entry.isDirectory ? .directory : .file,
            contentType: fileContentType(path: path),
            ext: ext,
            childCount: Int(filesCount))
        resource.mediaInformation = mediaInformation
        resource.formattedDuration = formattedDuration
        resource.subtitle = format(file: resource)
        
        
        return resource
    }
    
    fileprivate func getChildCount(entry: FileEntry) -> Int {
        if !entry.isDirectory { return 0 }
        return fileNativeWrapper.directoryElementsCount(entry.absolutePath, skipHidden: true)
    }
    
    
    func containsFile(path: String) -> Bool {
        return fileNativeWrapper.isExists(path)
    }
    
    
    func getUniqueName(for file: String, activeDownloadNames: [String]) -> String {
        var newFileName = concatWithDocumentsIfNeed(path: file)
        let originalFileExtension = (file as NSString).pathExtension
        var postfix = 1
        while containsFile(path: newFileName) || activeDownloadNames.contains(newFileName) {
            let ext = originalFileExtension.isEmpty ? "" : ".\(originalFileExtension)" 
            newFileName = "\(newFileName.withoutExtension)-\(postfix)\(ext)"
            postfix += 1
        }
        
        newFileName = newFileName
            .replacingOccurrences(of: rootPath(), with: "")
            .deletingSuffix("/")
            .deletingPrefix("/")
        return newFileName.nameWithExtension
    }
    
    func format(file: File) -> String {
        let formatter = byteCountFormatter
        var formatted = formatter.string(fromByteCount: file.size)
        if file.fileType == .directory {
            if file.childCount == 0 { formatted = L10n.Files.noFiles }
            else { formatted = L10n.Files.listItemFolderSubtitle(file.childCount, formatted) }
        }
        
        if file.contentType == .audio || file.contentType == .video {
            formatted = L10n.Files.simpleSubtitle(file.formattedDuration, formatted)
            if let bookmark = file.mediaInformation?.bookmark, bookmark > 0 {
                formatted = L10n.Files.mediaSubtitle(formatDuration(duration: bookmark), formatted)
            }
        }
        
        if let download = file.downloadItem {
            formatted = download.formatProgressString()
        }
        
        return formatted
    }
    
    func tempFilesDirectory() -> String {
        #if DEBUG
            return "tempDirectory"
        #else
            return ".tempDirectory"
        #endif
    }
    
    func segmentsFolder() -> String {
        return "\(tempFilesDirectory())/segments"
    }
    
    func directorySize(path: String) -> Int64 {
        return Int64(directoryInfo(path: path).size)
    }
    
    func starts(file: String, with folder: String) -> Bool {
        let documentedFilePath = concatWithDocumentsIfNeed(path: file)
        let documentedFolderPath = concatWithDocumentsIfNeed(path: folder)
        return documentedFilePath.starts(with: documentedFolderPath)
    }
}
