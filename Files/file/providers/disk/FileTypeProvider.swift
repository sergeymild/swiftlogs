//
//  FileTypeProvider.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 16/11/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation

func fileContentType(path: String?) -> FileContentType {
    guard let path = path else {
        return .file
    }
    return FileTypeProvider.getFileType(path: path)
}

fileprivate let videoExtensions = ["mp4", "mov", "avi", "mkv"]
class FileTypeProvider {
    static func getFileType(entry: FileEntry) -> FileContentType {
        //if entry.isDirectory { return .directory }
        return getFileType(path: entry.absolutePath)
    }
    
    static func getFileType(path: String) -> FileContentType {
        let ext = path.ext
        if ext == "zip" { return .zip }
        if videoExtensions.contains(ext) { return .video }
        if ext == "mp3" || ext == "wav" || ext == "aac" { return .audio }
        if ext == "jpg" || ext == "jpeg" || ext == "png" { return .image }
        if ext == "doc" || ext == "docx" { return .doc }
        if ext == "pdf" { return .pdf }
        return .file
    }
}
