//
//  Documents+Extensions.swift
//  Corie
//
//  Created by Sergei Golishnikov on 18/12/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation
import CommonExtensions

var didDocumentPathShow = false
var documentsPath: String {
    guard let documentsFolder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
        fatalError("Cant get documents folder")
    }
    if !didDocumentPathShow {
        didDocumentPathShow = true
        debugPrint(documentsFolder)
    }
    return documentsFolder
}

func concatWithDocumentsIfNeed(path: String) -> String {
    if path.starts(with: "dropbox:") { return path }
    let documents = documentsPath.deletingSuffix("/")
    var newFileName = path
    if !newFileName.starts(with: documents) {
        newFileName = "\(documents)/\(newFileName.deletingPrefix("/"))"
    }
    return newFileName.deletingSuffix("/")
}

func removeDocumentsPathIfNeed(path: String) -> String {
    return path.replacingOccurrences(of: documentsPath, with: "").deletingPrefix("/")
}
