//
//  Sorting.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 20/02/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation

enum ListSortType {
    case name
    case nameDesc
    case date
    case dateDesc
    case size
    case sizeDesc
    
}

class Sorting {
    var multiplier = 1
    
    func compareDownloads(lhs: File, rhs: File) -> Int {
        return -3
//        let lhsIsInHashes = lhs.downloadItem != nil
//        let rhsIsInHashes = rhs.downloadItem != nil
//        if lhsIsInHashes { return 1 }
//        if rhsIsInHashes { return -1 }
//        if lhsIsInHashes && rhsIsInHashes { return -3 }
//        return 0
    }
    
    func compareNames(l: String, r: String) -> Bool {
        switch l.lowercased().compare(r.lowercased()) {
        case .orderedAscending:
            return -1 * multiplier < 0
        case .orderedDescending:
            return 1 * multiplier < 0
        case .orderedSame:
            return 0 * multiplier < 0
        }
    }
    
    func sortByName(lhs: File, rhs: File) -> Bool {
//        let res = compareDownloads(lhs: lhs, rhs: rhs)
//        if res == 1 { return true }
//        if res == -1 { return false }
//        if res == -3 { return sortByDate(lhs: lhs, rhs: rhs) }

        if lhs.fileType == .iCloud && rhs.fileType != .iCloud { return true }
        if lhs.fileType != .iCloud && rhs.fileType == .iCloud { return false }
        if lhs.fileType == .dropbox && rhs.fileType != .dropbox { return true }
        if lhs.fileType != .dropbox && rhs.fileType == .dropbox { return false }
        if lhs.fileType == .yandexDisk && rhs.fileType != .yandexDisk { return true }
        if lhs.fileType != .yandexDisk && rhs.fileType == .yandexDisk { return false }
        if lhs.fileType == .directory && rhs.fileType != .directory { return true }
        if lhs.fileType != .directory && rhs.fileType == .directory { return false }
        return compareNames(l: lhs.name, r: rhs.name)
    }
    
    func sortBySize(lhs: File, rhs: File) -> Bool {
//        let res = compareDownloads(lhs: lhs, rhs: rhs)
//        if res == 1 { return true }
//        if res == -1 { return false }
//        if res == -3 { return sortByDate(lhs: lhs, rhs: rhs) }

        if lhs.fileType == .iCloud && rhs.fileType != .iCloud { return true }
        if lhs.fileType != .iCloud && rhs.fileType == .iCloud { return false }
        if lhs.fileType == .dropbox && rhs.fileType != .dropbox { return true }
        if lhs.fileType != .dropbox && rhs.fileType == .dropbox { return false }
        if lhs.fileType == .yandexDisk && rhs.fileType != .yandexDisk { return true }
        if lhs.fileType != .yandexDisk && rhs.fileType == .yandexDisk { return false }
        if lhs.fileType == .directory && rhs.fileType != .directory { return true }
        if lhs.fileType != .directory && rhs.fileType == .directory { return false }

        if (lhs.fileType == .directory && lhs.childCount == 0 && rhs.fileType == .directory && rhs.childCount == 0) {
            return compareNames(l: lhs.name, r: rhs.name)
        }
        
        return multiplier > 0 ? lhs.size < rhs.size : lhs.size > rhs.size
    }
    
    func sortByDate(lhs: File, rhs: File) -> Bool {
//        let res = compareDownloads(lhs: lhs, rhs: rhs)
//        if res == 1 { return true }
//        if res == -1 { return false }
//        if res == -3 { return sortByDate(lhs: lhs, rhs: rhs) }

        if lhs.fileType == .iCloud && rhs.fileType != .iCloud { return true }
        if lhs.fileType != .iCloud && rhs.fileType == .iCloud { return false }
        if lhs.fileType == .dropbox && rhs.fileType != .dropbox { return true }
        if lhs.fileType != .dropbox && rhs.fileType == .dropbox { return false }
        if lhs.fileType == .yandexDisk && rhs.fileType != .yandexDisk { return true }
        if lhs.fileType != .yandexDisk && rhs.fileType == .yandexDisk { return false }
        if lhs.fileType == .directory && rhs.fileType != .directory { return true }
        if lhs.fileType != .directory && rhs.fileType == .directory { return false }
        
        switch lhs.lastModificationTime.compare(rhs.lastModificationTime) {
        case .orderedAscending:
            return -1 * multiplier < 0
        case .orderedDescending:
            return 1 * multiplier < 0
        case .orderedSame:
            return 0 * multiplier < 0
        }
    }
    
    func sort(type: SortType, files: [File]) -> [File] {
        var sortedFiles = files
        switch type {
        case .name_asc:
            multiplier = 1
            sortedFiles = files.sorted(by: sortByName)
        case .name_desc:
            multiplier = -1
            sortedFiles = files.sorted(by: sortByName)
        case .size_asc:
            multiplier = 1
            sortedFiles = files.sorted(by: sortBySize)
        case .size_desc:
            multiplier = -1
            sortedFiles = files.sorted(by: sortBySize)
        case .date_asc:
            multiplier = 1
            sortedFiles = files.sorted(by: sortByDate)
        case .date_desc:
            multiplier = -1
            sortedFiles = files.sorted(by: sortByDate)
        }
        
        return sortedFiles
    }
}
